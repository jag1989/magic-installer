<?php
/**
 * Created by PhpStorm.
 * User: Jonathan
 * Date: 08/08/14
 * Time: 15:50
 */

// This function expects the input to be UTF-8 encoded.
function slugify($text)
{
    // Swap out Non "Letters" with a -
    $text = preg_replace('/[^\\pL\d]+/u', '-', $text);

    // Trim out extra -'s
    $text = trim($text, '-');

    // Convert letters that we have left to the closest ASCII representation
    $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

    // Make text lowercase
    $text = strtolower($text);

    // Strip out anything we haven't been able to convert
    $text = preg_replace('/[^-\w]+/', '', $text);

    return $text;
}

function recurse_copy($src,$dst) {
    $dir = opendir($src);
    @mkdir($dst);
    while(false !== ( $file = readdir($dir)) ) {
        if (( $file != '.' ) && ( $file != '..' )) {
            if ( is_dir($src . '/' . $file) ) {
                recurse_copy($src . '/' . $file,$dst . '/' . $file);
            }
            else {
                copy($src . '/' . $file,$dst . '/' . $file);
            }
        }
    }
    closedir($dir);
}


function recursive_remove_directory($directory, $empty=FALSE) {
    if(substr($directory,-1) == '/')
    {
        $directory = substr($directory,0,-1);
    }
    if(!file_exists($directory) || !is_dir($directory))
    {
        return FALSE;
    }elseif(is_readable($directory))
    {
        $handle = opendir($directory);
        while (FALSE !== ($item = readdir($handle)))
        {
            if($item != '.' && $item != '..')
            {
                $path = $directory.'/'.$item;
                if(is_dir($path))
                {
                    recursive_remove_directory($path);
                }else{
                    unlink($path);
                }
            }
        }
        closedir($handle);
        if($empty == FALSE)
        {
            if(!rmdir($directory))
            {
                return FALSE;
            }
        }
    }
    return TRUE;
}

if(isset($_POST['project_name']) && $_POST['project_name'] != NULL):

    $dirName = $_POST['project_name'];
    $dirName = slugify($dirName);
    $dirName = escapeshellcmd($dirName);
    $toExec = getcwd().DIRECTORY_SEPARATOR.'wp.sh';
    $latestWp = 'http://wordpress.org/latest.tar.gz';
    $wpFileName = 'latest.tar.gz';#
    $dbLocation = 'localhost';
    $dbUser = 'root';
    $dbPass = '';


    chdir('../');

    if (!file_exists(getcwd().DIRECTORY_SEPARATOR.$dirName)):

        /*Create dir*/
        mkdir($dirName);
        /*Move to created dir*/
        chdir($dirName);

        /*download latest WordPress via cURL*/
        $fp = fopen($wpFileName, 'w');
        $ch = curl_init($latestWp);
        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_exec($ch);
        curl_close($ch);
        fclose($fp);

        /*Unzip*/
        try {
            $phar = new PharData($wpFileName);
            $phar->extractTo(getcwd()); // extract all files
        } catch (Exception $e){};

        /*Remove gzip */
        unlink($wpFileName);

        /*move all files from wordpress up one level*/
        recurse_copy(getcwd().DIRECTORY_SEPARATOR.'wordpress', getcwd());
        recursive_remove_directory('wordpress');

        /*rename configuration*/
        rename('wp-config-sample.php', 'wp-config.php');

        /*Create DB Name*/
        $dbName = str_replace('-', '_', $dirName);

        /*Connect to DB*/

        $dbLink = mysqli_connect($dbLocation, $dbUser, $dbPass);
        if(!$dbLink):
            die('Could not connect: '. mysqli_error($dbLink));
        endif;

        $sql = "CREATE DATABASE ".$dbName;
        if(!mysqli_query($dbLink, $sql)):
            die("Error creating DB: ". mysqli_error($dbLink));
        endif;

        $wpConf = file_get_contents('wp-config.php');
        $wpConf = str_replace('database_name_here', $dbName, $wpConf);
        $wpConf = str_replace('username_here', $dbUser, $wpConf);
        $wpConf = str_replace('password_here', $dbPass, $wpConf);

        //$salts = file_get_contents('https://api.wordpress.org/secret-key/1.1/salt/');

        file_put_contents('wp-config.php', $wpConf);

        header( 'Location: http://localhost/'.$dirName ) ;

    else:
        echo 'Folder already Exists.';
    endif;

endif;
